#ifndef STRING_SPLITER
#define STRING_SPLITER

#include <string>
#include <vector>

typedef std::vector<std::string> stringVector;

class StringSplitter
{
private:
	StringSplitter();

public:
	enum splitBehavior
	{
		skipEmptyParts,
		keepEmptyParts
	};

public:
	static stringVector split(const std::string& stringToSplit, const std::string& separator, splitBehavior behavior = skipEmptyParts);
	static stringVector split(const std::string& stringToSplit, char separator, splitBehavior behavior = skipEmptyParts);
};

#endif //STRING_SPLITER