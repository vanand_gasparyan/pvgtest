#include "Result.h"

using namespace pvgtest;

Result::Result()
{
}

Result::Result(Manager::executionMode mode)
: m_mode(mode)
{
}

void Result::printResult() const
{
	if (m_mode == Manager::test)
		printDiffFiles();
	printErrors();
}

void Result::clean_all()
{
	for (auto elem : m_errors)
	{
		delete elem;
	}
	m_errors.erase(m_errors.begin(), m_errors.end());
	m_diffFiles.erase(m_diffFiles.begin(), m_diffFiles.end());
	m_mode = Manager::executuinModeMaxValue;
}

void Result::addError(Error* err)
{
	m_errors.push_back(err);
}

void Result::printErrors() const
{
	std::cout << "ERRORS:\n";
	if (m_errors.empty())
		std::cout << "No Error.\n";
	for (auto elem : m_errors)
		std::cout << elem->errorMessage();
}

void Result::addDiffFile(const std::string& fileName)
{
	m_diffFiles.insert(fileName);
}

void Result::printDiffFiles() const
{
	if (m_diffFiles.empty())
	{
		std::cout << "No differences appear in any file.\n";
	}
	else
	{
		std::cout << "differences found in following files:\n";
		for (auto elem : m_diffFiles)
			std::cout << elem << std::endl;
	}
}

