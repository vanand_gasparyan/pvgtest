#include <iostream>
#include <string>
#include <map>
#include <fstream>

#include "manager.h"
#include "Result.h"

int main(int argc, char* argv[])
{

	int index = 1;
	std::map<std::string, std::string> args_and_values;
	while (argv[index] && argv[index + 1])
	{
		args_and_values[argv[index]] = argv[index + 1];
		index += 2;
	}


	pvgtest::Manager mngr(pvgtest::Manager::test,
		args_and_values["-input_root"], 
		args_and_values["-golden_root"],
		args_and_values["-output_root"],
		args_and_values["-test_root"],
		args_and_values["-config"]);
	pvgtest::Result res = mngr.run();
	res.printResult();
}