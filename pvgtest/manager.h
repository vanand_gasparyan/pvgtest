#ifndef MANAGER
#define MANAGER

#include <string>
#include <vector>

#include "EasyBMP.h"

typedef std::vector<std::string> stringVector;

namespace pvgtest
{
	class Result;

	class Manager
	{
	private:
		enum generationMode
		{
			golden,
			output,
			generationModeMaxValue
		};
	public:
		enum executionMode
		{
			generate,
			test,
			executuinModeMaxValue
		};

	public:
		Manager(executionMode mode,
			const std::string& inputRoot = "",
			const std::string& goldenRoot = "",
			const std::string& outputRoot = "",
			const std::string& testRoot = "",
			const std::string& configFilename = "");
		Result run();

	private:
		void checkFileExistance_();
		void generate_(generationMode mode);
		void test_();
		std::string filenamePdfToBmp_(const std::string& old_filename);
		bool fileExists_(const std::string& fileName);
		inline std::wstring stringToWstring(const std::string& str) { return std::wstring(str.begin(), str.end()); }

		//bitmap file operations
	private:
		void toGreyscale(const BMP& image);//TODO
		
	public:
		void setMode(executionMode mode);
		void setInputRoot(const std::string& inputRoot);
		void setGoldenRoot(const std::string& goldenRoot);
		void setOutputRoot(const std::string& outputRoot);
		void setTestRoot(const std::string& testRoot);
		void setConfigFilename(const std::string& configFilename);

	private:
		executionMode m_mode;
		std::string m_inputRoot;
		std::string m_goldenRoot;
		std::string m_outputRoot;
		std::string m_testRoot;
		std::string m_configFilename;
		stringVector m_inputFiles;
		stringVector m_goldenFiles;
		stringVector m_outputFiles;
		stringVector m_testFiles;
		Result* m_result;
	};//class Manager

}//namespace pvgtest

#endif //MANAGER