#ifndef RESULT
#define RESULT

#include <iostream>
#include <string>
#include <vector>
#include <set>

#include "manager.h"

namespace pvgtest
{
	struct Error
	{
		virtual std::string errorMessage() = 0;
		virtual ~Error(){}
		//int errorID;
	};

	struct FileNotFoundError : public Error
	{
		FileNotFoundError(const std::string& fileName)
		: m_fileName(fileName)
		{}
		virtual std::string errorMessage()
		{
			return "File not found.\nFilename: " + m_fileName + "\n";
		}
	private:
		std::string m_fileName;
	};

	struct FileNotSavedError : public Error
	{
		FileNotSavedError(const std::string& fileName)
		: m_fileName(fileName)
		{}
		virtual std::string errorMessage()
		{
			return "File failed to save.\nFilename: " + m_fileName + "\n";
		}
	private:
		std::string m_fileName;
	};
	
	struct ArgumentMissingError : public Error
	{
		ArgumentMissingError(const std::string& argName)
		: m_argName(argName)
		{}
		virtual std::string errorMessage()
		{
			return m_argName + " argument missing.\n";
		}
	private:
		std::string m_argName;
	};

	typedef std::vector<Error*> ErrorContainer;
	typedef std::set<std::string> setString;

	class Result
	{
	public:
		Result();
		Result(Manager::executionMode mode);

		void printResult() const;
		void clean_all();

		// errors
	public:
		void addError(Error* err);
		void printErrors() const;
	private:
		ErrorContainer m_errors;

		// test results
	public:
		void addDiffFile(const  std::string& fileName);
		void printDiffFiles() const;
	private:
		setString m_diffFiles;

		// execution mode
	public:
		Manager::executionMode Mode() const { return m_mode; }
		void setMode(Manager::executionMode val) { m_mode = val; }
	private:
		Manager::executionMode m_mode;
	};

}//namespace pvgtest

#endif //RESULT