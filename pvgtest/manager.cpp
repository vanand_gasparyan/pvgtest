#include "manager.h"

#include <fstream>
#include <cassert>
#include <algorithm>

#include "StringSpliter.h"
#include "Result.h"

using namespace pvgtest;

Manager::Manager(Manager::executionMode mode,
	const std::string& inputRoot,
	const std::string& goldenRoot,
	const std::string& outputRoot,
	const std::string& testRoot,
	const std::string& configFilename)
	: m_mode(mode),
	m_inputRoot(inputRoot),
	m_goldenRoot(goldenRoot),
	m_outputRoot(outputRoot),
	m_testRoot(testRoot),
	m_configFilename(configFilename),
	m_result(0)
{
	assert(mode != Manager::generationMode::generationModeMaxValue);
	m_result = new Result(mode);

	//check_file_existance_();
	//if (m_args->at("-mode") == "generate")
	//{
	//	generate(generationMode::GOLDEN);
	//}
	//else if (m_args->at("-mode") == "test")
	//{
	//	generate(generationMode::OUTPUT);
	//	test();
	//}
}

Result Manager::run()
{	
	checkFileExistance_();

	switch (m_mode)
	{
	case pvgtest::Manager::generate:
		generate_(golden);
		break;
	case pvgtest::Manager::test:
		generate_(output);
		test_();
		break;
	case pvgtest::Manager::executuinModeMaxValue:
		m_result->addError(new ArgumentMissingError("-mode"));
		break;
	}
	return *m_result;
}

void Manager::checkFileExistance_()
{
	std::ifstream config(m_configFilename);
	std::string content = std::string(std::istreambuf_iterator<char>(config), std::istreambuf_iterator<char>());
	stringVector strVec = StringSplitter::split(content, "\n", StringSplitter::skipEmptyParts);

	for (auto item : strVec)
	{
		if (fileExists_(m_inputRoot + "\\" + item))
			m_inputFiles.push_back(item);
		else
			m_result->addError(new FileNotFoundError(m_inputRoot + "\\" + item));
	}

	if (m_mode == executionMode::test)
	{
		for (auto item : strVec)
		{
			std::string newFilename = m_goldenRoot + "\\" + filenamePdfToBmp_(item);
			if (fileExists_(newFilename))
				m_goldenFiles.push_back(filenamePdfToBmp_(item));
			else
				m_result->addError(new FileNotFoundError(newFilename));
		}
	}
}

void Manager::generate_(Manager::generationMode mode)
{
	//TODO
	switch (mode)
	{
	case pvgtest::Manager::generationMode::golden:
		//generate bmp files from pdf and save in golden_root		
		break;

	case pvgtest::Manager::generationMode::output:
		//generate bmp files from pdf and save in output_root
		break;

	case pvgtest::Manager::generationMode::generationModeMaxValue:
		/*std::cout << "Wrong argument value: void Manager::generate(Manager::generation_mode mode)\n";*/
		break;
	}
}

void Manager::test_()
{
	for (auto fileName : m_goldenFiles)
	{
		BMP goldenImage;
		if (!goldenImage.ReadFromFile(stringToWstring(m_goldenRoot + "\\" + fileName).c_str()))
		{
			m_result->addError(new FileNotFoundError(m_goldenRoot + "\\" + fileName));
		}

		BMP outputImage;
		if (!outputImage.ReadFromFile(stringToWstring(m_outputRoot + "\\" + fileName).c_str()))
		{
			m_result->addError(new FileNotFoundError(m_outputRoot + "\\" + fileName));
		}

		BMP testImage;
		int newWidth = std::min(goldenImage.TellWidth(), outputImage.TellWidth());
		int newHeight = std::min(goldenImage.TellHeight(), outputImage.TellHeight());
		testImage.SetSize(newWidth, newHeight);

		for (int i = 0; i < newWidth; ++i)
		{
			for (int j = 0; j < newHeight; ++j)
			{
				if (goldenImage(i, j)->Alpha == outputImage(i, j)->Alpha &&
					goldenImage(i, j)->Red == outputImage(i, j)->Red &&
					goldenImage(i, j)->Green == outputImage(i, j)->Green &&
					goldenImage(i, j)->Blue == outputImage(i, j)->Blue)
				{
					double Temp = 0.30*(goldenImage(i, j)->Red) +
						0.59*(goldenImage(i, j)->Green) +
						0.11*(goldenImage(i, j)->Blue);
					testImage(i, j)->Red = Temp;
					testImage(i, j)->Green = Temp;
					testImage(i, j)->Blue = Temp;
				}
				else
				{
					RGBApixel redPixel;
					redPixel.Alpha = 1.0;
					redPixel.Red = 255;
					redPixel.Blue = 0;
					redPixel.Green = 0;
					testImage.SetPixel(i, j, redPixel);
					m_result->addDiffFile(fileName);
				}
			}
		}
		if (!testImage.WriteToFile(stringToWstring(m_testRoot + "\\" + fileName).c_str()))
		{
			m_result->addError(new FileNotSavedError(m_testRoot + "\\" + fileName));
		}
	}
}

std::string Manager::filenamePdfToBmp_(const std::string& oldFilename)
{
	std::string newFilename;
	stringVector strVec = StringSplitter::split(oldFilename, '.', StringSplitter::skipEmptyParts);
	int lastIndex = strVec.size() - 1;
	for (int i = 0; i < lastIndex; ++i)
	{
		newFilename += strVec[i] + ".";
	}
	newFilename += "bmp";
	return newFilename;
}

bool Manager::fileExists_(const std::string& fileName)
{
	std::fstream tmpFstr;
	tmpFstr.open(fileName);
	return tmpFstr.is_open();
}

//setters

void Manager::setMode(Manager::executionMode mode)
{
	m_mode = mode;
}

void Manager::setInputRoot(const std::string& inputRoot)
{
	m_inputRoot = inputRoot;
}

void Manager::setGoldenRoot(const std::string& goldenRoot)
{
	m_goldenRoot = goldenRoot;
}

void Manager::setOutputRoot(const std::string& outputRoot)
{
	m_outputRoot = outputRoot;
}

void Manager::setTestRoot(const std::string& testRoot)
{
	m_testRoot = testRoot;
}

void Manager::setConfigFilename(const std::string& configFilename)
{
	m_configFilename = configFilename;
}
