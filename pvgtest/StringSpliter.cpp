#include "StringSpliter.h"

StringSplitter::StringSplitter()
{
}

stringVector StringSplitter::split(const std::string& stringToSplit, const std::string& separator, splitBehavior behavior)
{
	stringVector result;	
	int strSize = stringToSplit.size();
	int sepSize = separator.size();

	int strIndex = 0;
	while (strIndex != strSize)
	{
		std::string tmpString = "";
		int sepIndex = 0;

		while (sepIndex != sepSize)
		{
			if (strIndex == strSize)
				break;

			tmpString.push_back(stringToSplit[strIndex]);
			(stringToSplit[strIndex] != separator[sepIndex]) ? (sepIndex = 0) : (++sepIndex);

			++strIndex;
		}

		if (strIndex != strSize)
			tmpString.resize(tmpString.size() - sepSize);
		
		switch (behavior)
		{
		case splitBehavior::skipEmptyParts:
			if (!tmpString.empty())
				result.push_back(tmpString);
			break;
		case splitBehavior::keepEmptyParts:
			result.push_back(tmpString);
			break;
		}
	}

	return result;
}

stringVector StringSplitter::split(const std::string& stringToSplit, char separator, splitBehavior behavior)
{
	std::string newSeparator;
	newSeparator.push_back(separator);
	return split(stringToSplit, newSeparator, behavior);
}